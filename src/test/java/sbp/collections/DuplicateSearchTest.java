package sbp.collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;
import java.util.List;

public class DuplicateSearchTest {
    private List<String> myList = new ArrayList<>();
    private List<String> resultList = new ArrayList<>();

    /**
     * @param myList - заполнение исходной коллекции.
     * @param resultList - заполнение коллекции дубликатов для сверки.
     */
    @Before
    public void setTestList() {
        myList.add("a");
        myList.add("c");
        myList.add("g");
        myList.add("a");
        myList.add("t");
        myList.add("a");
        myList.add("t");
        myList.add("e");
        myList.add("e");
        myList.add("k");
        myList.add("g");

        resultList.add("a");
        resultList.add("g");
        resultList.add("t");
        resultList.add("e");
    }

    /**
     * Проверка коллекции на соответвие коллекции дубликатов
     * методом перебора (2 метода)
     */
    @Test
    public void getDuplicateOneTest() {
        Assertions.assertTrue(resultList.containsAll(DuplicateSearch.duplicateSearch(myList)));
        Assertions.assertTrue(resultList.containsAll(DuplicateSearch.duplicateSearchWithoutSet(myList)));
    }

    /**
     * Проверка коллекции на соответвие коллекции дубликатов
     * с использованием Map
     */
    @Test
    public void getDuplicateTwoTest() {
        Assertions.assertTrue(resultList.containsAll(DuplicateSearch.duplicateSearchWithoutMap(myList)));
    }

    /**
     * Проверка коллекции на соответвие коллекции дубликатов
     * с использованием stream:
     * 1 - используя Set
     * 2 - используя Map
     * 3 - группировкой
     * 4 - частотойt
     */
    @Test
    public void getDuplicateSearchWithoutStream() {
        Assertions.assertTrue(resultList.containsAll(DuplicateSearch.duplicateSearchWithoutStreamFromSet(myList)));
        Assertions.assertTrue(resultList.containsAll(DuplicateSearch.duplicateSearchWithoutStreamFromMap(myList)));
        Assertions.assertTrue(resultList.containsAll(DuplicateSearch.duplicateSearchWithoutStreamFromGrouping(myList)));
        Assertions.assertTrue(resultList.containsAll(DuplicateSearch.duplicateSearchWithoutStreamFromFrequency(myList)));
    }
}
