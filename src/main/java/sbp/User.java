package sbp;

/**
 * @author Nochkin Evgeniy
 * @version 1.0
 * Класс пользователь.
 */
public class User {

    private String firstName;
    private String lastName;
    private int age;
    private boolean sex;

    public User()
    {
        this("", "", 0, true);
    }

    /**
     * @param firstName - Имя
     * @param lastName - Фамилия
     * @param age - Возраст
     * @param sex - пол (муж. - true)
     */
    public User(String firstName, String lastName, int age, boolean sex)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.sex = sex;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        User user = (User) o;
        return this.firstName.equals(user.firstName) & this.lastName.equals(user.lastName)
                & this.age == user.age & this.sex == user.sex;
    }

    @Override
    public int hashCode()
    {
        int result = firstName == null ? 0 : firstName.hashCode();
        result = 31 * result + lastName == null ? 0 : lastName.hashCode();
        result = 31 * result + age;
        result = 31 * result + new Boolean(sex).hashCode();
        return result;
    }

    @Override
    public String toString()
    {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", sex=" + sex +
                '}';
    }
}
