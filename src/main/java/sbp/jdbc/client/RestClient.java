package sbp.jdbc.client;

import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import sbp.jdbc.model.Person;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Nochkin Evgeniy
 * @version 1.0
 * Rest-клиент взаимодействия с сервером "http://localhost:8080/spring-rest".
 */
public class RestClient {

    private static final String CREATE_PERSON = "http://localhost:8080/spring-rest/person";
    private static final String READ_ALL_PERSONS = "http://localhost:8080/spring-rest/home";
    private static final String READ_PERSON_BY_ID = "http://localhost:8080/spring-rest/person/{id}";
    private static final String UPDATE_PERSON = "http://localhost:8080/spring-rest/person/{id}";
    private static final String DELETE_PERSON = "http://localhost:8080/spring-rest/person/{id}";

    static RestTemplate restTemplate = new RestTemplate();

    public static void main(String[] args) {
        getAllUsers();

        getPersonById(10);

        Person person = new Person("Aleksey", "Baden-Baden", 43);
        createPerson(person);

        Person uPerson = new Person("Anastaisha", "Toronto", 37);
        updatePerson(uPerson, 20);

        deletePerson(17);
    }

    /**
     * Получение всех пользователей из базы данных
     * и вывод их в терминал в формате JSON.
     */
    private static void getAllUsers() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<>("params", headers);
        ResponseEntity<String> result = restTemplate.exchange(READ_ALL_PERSONS, HttpMethod.GET, entity, String.class);

        System.out.println(result);
    }

    /**
     * Добавление нового пользователя в базу данных.
     * @param person новый пользователь.
     */
    private static void createPerson(Person person) {
        restTemplate.postForEntity(CREATE_PERSON, person, Person.class);
    }

    /**
     * Получение пользователя из базы данных по Id.
     * @param id пользователя.
     */
    private static void getPersonById(Integer id) {
        Map<String, Integer> param = new HashMap<>();
        param.put("id", id);

        Person person = restTemplate.getForObject(READ_PERSON_BY_ID, Person.class, param);
        System.out.println("Имя: " + person.getPersonName() +
                ", город: " + person.getCity() +
                ", возраст: " + person.getAge());
    }

    /**
     * Изменения данных о пользователе по id.
     * @param person новые данные.
     * @param id изменяемого пользователя.
     */
    private static void updatePerson(Person person, Integer id) {
        Map<String, Integer> param = new HashMap<>();
        param.put("id", id);
        Person oldPerson = restTemplate.getForObject(READ_PERSON_BY_ID, Person.class, param);

        oldPerson.setPersonName(person.getPersonName());
        oldPerson.setCity(person.getCity());
        oldPerson.setAge(person.getAge());

        restTemplate.put(UPDATE_PERSON, oldPerson, id);
    }

    /**
     * Удаление пользователя по id.
     * @param id удаляемого пользователя.
     */
    private static void deletePerson(Integer id) {
        restTemplate.delete(DELETE_PERSON, id);
    }
}
