package sbp.jdbc.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sbp.jdbc.dao.PersonRepo;
import sbp.jdbc.dao.PersonRepoImpl;
import sbp.jdbc.exceptions.ResourceNotFoundException;
import sbp.jdbc.model.Person;

import java.util.List;

/**
 * @author Nochkin Evgeniy
 * @version 1.0
 * Обработка запросов по адресу /spring-rest.
 */
@RestController
@RequestMapping("/spring-rest")
public class PersonController {

    @Autowired
    private PersonRepo personRepo = new PersonRepoImpl();

    /**
     * Получение списка Person из базы данных.
     * @return список Person.
     */
    @GetMapping("/home")
    public ResponseEntity<List<Person>> startController() {
        List<Person> list = personRepo.findAllPerson();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    /**
     * Получение Person по id из базы данных.
     * @param id записи.
     * @return Person.
     */
    @GetMapping("/person/{id}")
    public ResponseEntity<Person> getPersonController(@PathVariable (value = "id") int id) {
        Person person = personRepo.findPersonById(id);
        if (person == null) {
            new ResourceNotFoundException("Person not found");
        }
        return new ResponseEntity<>(person, HttpStatus.OK);
    }

    /**
     * Добавление Person в базу данных.
     * @param person новый Person.
     */
    @PostMapping("/person")
    public void postPersonController(@RequestBody Person person) {
        personRepo.createPerson(person);
    }

    /**
     * Изменение Person в базе данных.
     * @param person новый Person.
     * @param id изменяемой записи.
     */
    @PutMapping("/person/{id}")
    public void putPersonController(@RequestBody Person person, @PathVariable (value = "id") int id) {
        Person personOld = personRepo.findPersonById(id);
        if (person == null) {
            new ResourceNotFoundException("Person not found");
        }

        personOld.setPersonName(person.getPersonName());
        personOld.setCity(person.getCity());
        personOld.setAge(person.getAge());
        personRepo.updatePerson(personOld);
    }

    /**
     * Удаление Person из базы данных по id.
     * @param id удаляемой записи.
     */
    @DeleteMapping("/person/{id}")
    public void deletePersonController(@PathVariable (value = "id") int id) {
        personRepo.deletePerson(id);
    }
}
