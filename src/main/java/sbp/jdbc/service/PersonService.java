package sbp.jdbc.service;

import sbp.jdbc.model.Person;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * @author Nochkin Evgeniy
 * @version 2.0
 * Класс взаимодействия с базой данных для работы с {@link sbp.jdbc.model.Person}
 */
public class PersonService {

    private Connection connection;
    private PreparedStatement preparedStatement;
    private Properties property = new Properties();

    /**
     * База данных с которой осуществляется взаимодействие
     */
    public String getJdbcUrl() {
        try (FileInputStream fis = new FileInputStream("C:/projects/lecture4/src/main/resources/config.properties")) {
            property.load(fis);
            String pr = property.getProperty("db.host");
            return pr;
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * Установка соединения с базой данных
     * @return соединение если удачно, иначе null
     */
    public void buildPreparedStatement(String query) {
        try {
            Class.forName("org.sqlite.JDBC");
            this.connection = DriverManager.getConnection(getJdbcUrl());
            this.preparedStatement = connection.prepareStatement(query);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Закрытие соединения с базой данных
     */
    public void close() {
        try {
            this.preparedStatement.close();
            this.connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Поиск записей в базе данных
     * @param query - SQL запрос SELECT
     * @return список {@link Person} найденных согласно запросу
     */
    public List<Person> executeQuery(String query) {
        List<Person> list = new ArrayList<>();
        try {
            buildPreparedStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next())
            {
                Person person = new Person(resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getInt(4));

                list.add(person);
            }

            close();
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Потокобезопасное внесение изменений в базу данных
     * @param query - SQL запрос на INSERT, UPDATE, DELETE
     * @return 0 если нет изменений, иначе количество измененных записей
     */
    public synchronized int executeUpdate(String query) {

        try {
            buildPreparedStatement(query);
            int result = preparedStatement.executeUpdate();
            close();
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }
}
