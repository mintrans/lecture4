package sbp.jdbc;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import sbp.jdbc.dao.PersonRepoImpl;
import sbp.jdbc.model.Person;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Тестирование контроллеров по адресу "/spring-rest"
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class PersonControllerTest {
    Person person1, person2, person3, person22;
    PersonRepoImpl mockPersonRepo;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper mapper;

    @Before
    public void start() {
        person1 = new Person(1, "One", "Moscow", 21);
        person2 = new Person(2, "Two", "Tver", 22);
        person3 = new Person(3, "Three", "Novgorod", 23);
        person22 = new Person(2, "TwoTwo", "Trerrr", 32);

        mockPersonRepo = mock(PersonRepoImpl.class);
    }

    /**
     * Тестирование контролера вывода на экран всех записей.
     * @throws Exception
     */
    @Test
    public void startControllerTest() throws Exception {
        List<Person> records = new ArrayList(Arrays.asList(person1, person2, person3));

        Mockito.when(mockPersonRepo.findAllPerson()).thenReturn(records);

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/spring-rest/home")
                        .contentType(MediaType.APPLICATION_JSON))
                        .andExpect(status().isOk());
    }

    /**
     * Тестирование контролера получения записи пользователя по id.
     * @throws Exception
     */
    @Test
    public void getPersonControllerTest() throws Exception {
        Mockito.when(mockPersonRepo.findPersonById(1)).thenReturn(person1);

        mockMvc.perform(MockMvcRequestBuilders
                .get("/spring-rest/person/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    /**
     * Тестирование контроллера внесения пользователя в базу данных.
     * @throws Exception
     */
    @Test
    public void postPersonController() throws Exception {
        Mockito.when(mockPersonRepo.createPerson(person1)).thenReturn(true);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post("/spring-rest/person")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(this.mapper.writeValueAsString(person1));

        mockMvc.perform(mockRequest).andExpect(status().isOk());
    }

    /**
     * Тестирование контроллера изменения пользователя.
     * @throws Exception
     */
    @Test
    public void putPersonController() throws Exception {
        Mockito.when(mockPersonRepo.findPersonById(2)).thenReturn(person2);

        Mockito.when(mockPersonRepo.updatePerson(person2)).thenReturn(true);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.put("/spring-rest/person/2")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(this.mapper.writeValueAsString(person2));

        mockMvc.perform(mockRequest).andExpect(status().isOk());
    }

    /**
     * Тестирование контролера удаления пользователя по id.
     * @throws Exception
     */
    @Test
    public void deletePersonController() throws Exception {
        Mockito.when(mockPersonRepo.deletePerson(3)).thenReturn(true);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.delete("/spring-rest/person/3")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(this.mapper.writeValueAsString(person3));

        mockMvc.perform(mockRequest).andExpect(status().isOk());
    }
}
