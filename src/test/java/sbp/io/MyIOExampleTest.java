package sbp.io;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;

public class MyIOExampleTest
{
    public final MyIOExample myIOExample = new MyIOExample();

    /**
     * Тест если:
     * - отсутвует физически
     * - является директорией
     * - является файлом
     * @throws IOException
     */
    @Test
    public void workWithFileTest() throws IOException {
        Assertions.assertFalse(myIOExample.workWithFile("C:\\NotExistDir"));
        Assertions.assertFalse(myIOExample.workWithFile("C:\\Progects"));
        Assertions.assertTrue(myIOExample.workWithFile("C:\\Projects\\Test2.txt"));
    }

    /**
     * Тест если:
     * - отсутвует физически
     * - является директорией
     * - является файлом
     * @throws IOException
     */
    @Test
    public void workWithFileNIOTest() throws IOException {
        Assertions.assertFalse(myIOExample.workWithFileNIO("C:\\NotExistDir"));
        Assertions.assertFalse(myIOExample.workWithFileNIO("C:\\Projects"));
        Assertions.assertTrue(myIOExample.workWithFileNIO("C:\\Projects\\Test2.txt"));
    }

    /**
     * Тест если:
     * - переданые данные не являются файлом
     * - копия файла успешно создана
     * @throws IOException
     */
    @Test
    public void copyFileTest() throws IOException
    {
        Assertions.assertFalse(myIOExample.copyFile("C:\\Projects\\Test5.txt", "C:\\Projects\\TestCopy5.txt"));
        Assertions.assertFalse(myIOExample.copyFile("C:\\Projects", "C:\\Projects\\TestCopy5.txt"));
        Assertions.assertTrue(myIOExample.copyFile("C:\\Projects\\Test2.txt", "C:\\Projects\\TestCopy2.txt"));
    }

    /**
     * Тест если:
     * - переданые данные не являются файлом (2 теста)
     * - копия файла успешно создана
     * @throws IOException
     */
    @Test
    public void copyFileNIOTest() throws IOException
    {
        Assertions.assertFalse(myIOExample.copyFileNIO("C:\\Projects\\Test5.txt", "C:\\Projects\\TestCopy5.txt"));
        Assertions.assertFalse(myIOExample.copyFileNIO("C:\\Projects", "C:\\Projects\\TestCopy5.txt"));
        Assertions.assertTrue(myIOExample.copyFileNIO("C:\\Projects\\Test2.txt", "C:\\Projects\\TestCopy2.txt"));
    }

    /**
     * - переданые данные не являются файлом
     * - копия файла успешно создана
     * @throws IOException
     */
    @Test
    public void copyBufferedFileText() throws IOException
    {
        Assertions.assertFalse(myIOExample.copyFile("C:\\Projects\\Test5.txt", "C:\\Projects\\TestCopy5.txt"));
        Assertions.assertFalse(myIOExample.copyFile("C:\\Projects", "C:\\Projects\\TestCopy5.txt"));
        Assertions.assertTrue(myIOExample.copyFile("C:\\Projects\\Test2.txt", "C:\\Projects\\TestCopy2.txt"));
    }

    /**
     * - переданые данные не являются файлом
     * - копия файла успешно создана
     * @throws IOException
     */
    @Test
    public void copyFileWithReaderAndWriterTest() throws IOException
    {
        Assertions.assertFalse(myIOExample.copyFileWithReaderAndWriter("C:\\Projects\\Test5.txt", "C:\\Projects\\TestCopy5.txt"));
        Assertions.assertFalse(myIOExample.copyFile("C:\\Projects", "C:\\Projects\\TestCopy5.txt"));
        Assertions.assertTrue(myIOExample.copyFileWithReaderAndWriter("C:\\Projects\\Test2.txt", "C:\\Projects\\TestCopy2.txt"));
    }

    /**
     * - переданые данные не являются файлом (2 теста)
     * - копия файла успешно создана
     * @throws IOException
     */
    @Test
    public void copyFileWitInputOutputStreamNIOTest() throws IOException
    {
        Assertions.assertFalse(myIOExample.copyFileWithInputOutputStreamNIO("C:\\Projects\\Test5.txt", "C:\\Projects\\TestCopy5.txt"));
        Assertions.assertFalse(myIOExample.copyFileWithInputOutputStreamNIO("C:\\Projects", "C:\\Projects\\TestCopy5.txt"));
        Assertions.assertTrue(myIOExample.copyFileWithInputOutputStreamNIO("C:\\Projects\\Test2.txt", "C:\\Projects\\TestCopy2.txt"));
    }

    /**
     * - переданые данные не являются файлом (2 теста)
     * - копия файла успешно создана
     * @throws IOException
     */
    @Test
    public void copyBufferedFileTextNIO() throws IOException
    {
        Assertions.assertFalse(myIOExample.copyFileBufferedNIO("C:\\Projects\\Test5.txt", "C:\\Projects\\TestCopy5.txt"));
        Assertions.assertFalse(myIOExample.copyFileBufferedNIO("C:\\Projects", "C:\\Projects\\TestCopy5.txt"));
        Assertions.assertTrue(myIOExample.copyFileBufferedNIO("C:\\Projects\\Test2.txt", "C:\\Projects\\TestCopy2.txt"));
    }
}
