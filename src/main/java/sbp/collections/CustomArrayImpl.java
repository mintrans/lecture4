package sbp.collections;

import java.util.Arrays;
import java.util.Collection;

/**
 * @author Nochkin Evgeniy
 * @version 1.0
 * @param <T>
 */
public class CustomArrayImpl<T> implements CustomArray<T> {
    public Object[] array;

    private static final int DEFAULT_CAPACITY = 16;
    private static final Object[] DEFAULT_ARRAY = {};

    //Конструктор по умолчанию
    public CustomArrayImpl() {
        this.array = DEFAULT_ARRAY;
    }

    //Конструктор с указанием объема массива
    public CustomArrayImpl(int capacity) {
        this.array = new Object[capacity];
    }

    //Конструктор с созданием массива из коллекции
    public CustomArrayImpl(Collection<T> c) {
        array = c.toArray();
    }

    //Размер массива
    @Override
    public int size() {
        if (array.length < 0) return 0;

        int s = 0;
        for (Object ob : array)
        {
            if (ob != null) s++;
        }

        return s;
    }

    //Проверка на наличие элементов
    @Override
    public boolean isEmpty() {
        return (this.size() == 0);
    }

    //Добавление нового элемента
    @Override
    public boolean add(T item) {
        if (this.size() == this.getCapacity())
            this.array = newArray(array);

        for (int i = 0; i < array.length; i++)
        {
            if (array[i] == null)
            {
                array[i] = item;
                break;
            }
        }
        return true;
    }

    //Добавление массива элементов
    @Override
    public boolean addAll(T[] items) {
        int sizeInputArray = this.getSize(items);
        if (sizeInputArray == 0) return false;

        int newSize = this.size() + sizeInputArray;
        if (newSize > this.getCapacity())
            this.ensureCapacity(newSize);

        int j = 0;
        for (int i = this.size(); i < newSize; i++)
        {
            array[i] = items[j];
            j++;
        }

        return true;
    }

    //Добавление коллекции элементов
    @Override
    public boolean addAll(Collection<T> items) {
        this.addAll((T[]) items.toArray());
        return true;
    }

    //Добавление массива элементов с индекса
    @Override
    public boolean addAll(int index, T[] items) {
        this.addAll(Arrays.copyOfRange(items, index, items.length));
        return true;
    }

    //Получить элемент по индексу
    @Override
    public T get(int index) {
        if (index >= this.size()) {
            System.out.println("Недопустимы индекс!");
            return null;
        }

        return (T) array[index];
    }

    //Установка элемента по индексу с заменой имеющегося
    @Override
    public T set(int index, T item) {
        T old = null;
        try {
            old = this.get(index);
            array[index] = item;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return old;
    }

    //Удаление элемента по индексу
    @Override
    public void remove(int index) {
        array[index] = null;
        for (int i = index; i < this.size(); i++)
        {
            if (array[i + 1] != null)
            {
                array[i] = array[i + 1];
                array[i + 1] = null;
            }
            else
            {
                break;
            }
        }
    }

    //Удаление элемента по значению
    @Override
    public boolean remove(T item) {
        for (int i = 0; i < this.size(); i++)
        {
            if (((T) array[i]).equals(item))
            {
                this.remove(i);
                return true;
            }
        }
        return false;
    }

    //Проверка на наличие элемента
    @Override
    public boolean contains(T item) {
        for (int i = 0; i < this.size(); i++)
        {
            if (((T) array[i]).equals(item)) return true;
        }
        return false;
    }

    //Поиск индекса элемента
    @Override
    public int indexOf(T item) {
        int result = -1;
        for (int i = 0; i < this.size(); i++)
        {
            if (((T) array[i]).equals(item))
            {
                result = i;
                break;
            }
        }
        return result;
    }

    //Изменение объема массива
    @Override
    public void ensureCapacity(int newElementsCount) {
        Object[] newArray = new Object[newElementsCount];
        for (int i = 0; i < this.size() & i < newArray.length; i++)
        {
            newArray[i] = array[i];
        }
        array = newArray;
    }

    //Получить объем массива
    @Override
    public int getCapacity() {
        return array.length;
    }

    //Развернуть массив
    @Override
    public void reverse() {
        Object[] newArray = new Object[this.getCapacity()];
        int size = this.size();
        for (int i = 0; i < size; i++)
        {
            newArray[i] = array[size - 1 - i];
        }
        array = newArray;
    }

    //Преобразовать массив в array
    @Override
    public Object[] toArray() {
        return array;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CustomArrayImpl)) return false;
        CustomArrayImpl<?> that = (CustomArrayImpl<?>) o;
        return Arrays.equals(array, that.array);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(array);
    }

    @Override
    public String toString() {
        String result = "[ ";
        for (int i = 0; i < this.size(); i++)
        {
            result = result + this.get(i) + " ";
        }
        return result + "]";
    }

    //Увеличение объема массива на константу
    private Object[] newArray(Object[] array) {
        Object[] newArray = new Object[array.length + DEFAULT_CAPACITY];
        for (int i = 0; i < array.length; i++)
        {
            newArray[i] = array[i];
        }
        return newArray;
    }

    //Определяет размер массива
    private int getSize(T[] array) {
        int result = 0;
        for (int i = 0; i < array.length; i++)
        {
            if (array[i] != null) result++;
        }
        return result;
    }
}
