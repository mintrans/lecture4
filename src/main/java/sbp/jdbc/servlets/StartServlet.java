package sbp.jdbc.servlets;

import sbp.jdbc.dao.PersonRepo;
import sbp.jdbc.dao.PersonRepoImpl;
import sbp.jdbc.model.Person;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author Nochkin Evgeniy
 * @version 1.0
 * Сервлет обработки стартовой страницы
 */
@WebServlet("/")
public class StartServlet extends HttpServlet {

    PersonRepo personRepo = new PersonRepoImpl();

    /**
     * Обработка GET запроса.
     * Вывод всех пользователей из базы данных.
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        List<Person> list = personRepo.findAllPerson();
        req.setAttribute("personsList", list);
        getServletContext().getRequestDispatcher("/index.jsp").forward(req, resp);
    }
}