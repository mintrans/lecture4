package sbp;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class IsRightOrderHooksTest {

    /**
     * Тест метода isRightOrderHooksMethod() по проверки корректности строки скобок
     */
    @Test
    public void isRightOrderHooksMethodTest() {

        Assertions.assertFalse(IsRightOrderHooks.isRightOrderHooksMethod(""));

        Assertions.assertFalse(IsRightOrderHooks.isRightOrderHooksMethod("[(])"));
        Assertions.assertFalse(IsRightOrderHooks.isRightOrderHooksMethod("{"));
        Assertions.assertFalse(IsRightOrderHooks.isRightOrderHooksMethod(")"));
        Assertions.assertFalse(IsRightOrderHooks.isRightOrderHooksMethod("({[][({((()))})(])]})"));
        Assertions.assertFalse(IsRightOrderHooks.isRightOrderHooksMethod("{({[}(])})"));

        Assertions.assertTrue(IsRightOrderHooks.isRightOrderHooksMethod("(){}[]"));
        Assertions.assertTrue(IsRightOrderHooks.isRightOrderHooksMethod("{([({})])}"));
        Assertions.assertTrue(IsRightOrderHooks.isRightOrderHooksMethod("(()()())"));
    }
}
