package sbp;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class UserTest {

    /**
     * Тестирование класса пользователя при пустом конструкторе.
     */
    @Test
    public void newUserTest1()
    {
        User user = new User();

        Assertions.assertEquals(user.getFirstName(), "");
        Assertions.assertEquals(user.getLastName(), "");
        Assertions.assertEquals(user.getAge(), 0);
        Assertions.assertEquals(user.isSex(), true);
    }

    /**
     * Тестирование класса пользоателя при наличии входных данных.
     */
    @Test
    public void newUserTest2()
    {
        User user = new User("Mary", "Ivanova", 25, false);

        Assertions.assertEquals(user.getFirstName(), "Mary");
        Assertions.assertEquals(user.getLastName(), "Ivanova");
        Assertions.assertEquals(user.getAge(), 25);
        Assertions.assertEquals(user.isSex(), false);
    }
}
