package sbp.exceptions;

public class MyException extends Exception {

    public MyException(String mes)
    {
        super(mes);
    }

    public MyException(String mes, Throwable throwable) {
        super(mes, throwable);
    }
}
