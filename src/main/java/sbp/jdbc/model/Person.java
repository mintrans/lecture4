package sbp.jdbc.model;

import javax.persistence.*;
import java.util.Objects;


/**
 * @author Nochkin Evgeniy
 * @version 3.0
 */
@Entity
@Table(name="persons")
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "personName")
    private String personName;

    @Column(name = "city")
    private String city;

    @Column(name="age")
    private int age;

    public Person() {
        this.id = 0;
        this.personName = "NoName";
        this.city = "NoCity";
        this.age = 0;
    }

    public Person(String personName, String city, int age) {
        this.id = 0;
        this.personName = personName;
        this.city = city;
        this.age = age;
    }

    public Person(int id, String personName, String city, int age) {
        this.id = id;
        this.personName = personName;
        this.city = city;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof Person)) return false;
        Person person = (Person) o;
        return age == person.age && Objects.equals(personName, person.personName) && Objects.equals(city, person.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(personName, city, age);
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + personName + '\'' +
                ", city='" + city + '\'' +
                ", age=" + age +
                '}';
    }
}
