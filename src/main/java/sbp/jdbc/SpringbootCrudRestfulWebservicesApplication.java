package sbp.jdbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Nochkin Evgeniy
 * @version 1.0
 * Spring-сервер
 */
@SpringBootApplication
public class SpringbootCrudRestfulWebservicesApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootCrudRestfulWebservicesApplication.class, args);
    }
}
