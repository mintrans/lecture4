package sbp.comparators;

import java.util.Comparator;

/**
 * @author Nochkin Evgeniy
 * @version 1.0
 */
public class CustomDigitComparator implements Comparator<Integer> {

    /**
     * Сортировка - в начало четные, далее нечетные числа. По возрастанию.
     * @param o1 - число 1
     * @param o2 = число 2
     * @return 0 если числа равны.
     * Если оба числа четные/нечетные, то -1 если первое меньше второго. Иначе 1.
     * Если первое число четное, а второе нечетное, то -1. Тначе 1.
     */
    @Override
    public int compare(Integer o1, Integer o2) {
        if (o1.equals(o2)) return 0;
        if ((o1 % 2 == 0 & o2 % 2 == 0) | (o1 % 2 != 0 & o2 % 2 != 0)) {
            if (o1 > o2) return 1;
            else return -1;
        } else if (o1 % 2 == 0) return -1;
        else return 1;
    }
}
