package sbp.collections;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.List;

public class CustomArrayImplTest {

    /**
     * Созданых новых объектов класса CustomArrayImpl
     * - без аргументов
     * - с указанием размера
     * - без аргументов с добавлением элементов
     * - на основе коллекции ArrayList
     */
    @Test
    public void newCustomArrayTest() {
        //Пустой массив
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();

        Assertions.assertEquals(0, customArray.getCapacity());
        Assertions.assertEquals(0, customArray.size());

        //Пустой массив с заданым объемом
        CustomArrayImpl<Integer> customArray2 = new CustomArrayImpl<>(33);

        Assertions.assertEquals(33, customArray2.getCapacity());
        Assertions.assertEquals(0, customArray2.size());

        //Пустой массив с добавдением элементов
        CustomArrayImpl<Integer> customArray3 = new CustomArrayImpl<>();

        customArray3.add(111);
        customArray3.add(222);
        customArray3.add(333);

        Assertions.assertEquals(16, customArray3.getCapacity());
        Assertions.assertEquals(3, customArray3.size());

        //На основе Collections
        List<Integer> list = new ArrayList<>();
        list.add(111);
        list.add(222);
        list.add(333);

        CustomArrayImpl<Integer> customArray4 = new CustomArrayImpl<>(list);

        Assertions.assertEquals(111, customArray4.get(0));
        Assertions.assertEquals(222, customArray4.get(1));
        Assertions.assertEquals(333, customArray4.get(2));
    }

    /**
     * Размер массива:
     * - новый == 0
     * - с тремя элементами == 3
     */
    @Test
    public void sizeTest() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();

        Assertions.assertEquals(0, customArray.size());

        customArray.add(111);
        customArray.add(222);
        customArray.add(333);

        Assertions.assertEquals(3, customArray.size());
    }

    /**
     * Наличие элементов в массиве
     * - в новом == true
     * - в заполненном == false
     */
    @Test
    public void isEmptyTest() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();

        Assertions.assertTrue(customArray.isEmpty());

        customArray.add(111);
        customArray.add(222);
        customArray.add(333);

        Assertions.assertFalse(customArray.isEmpty());
    }

    /**
     * Добавление элемента в массив
     */
    @Test
    public void addTest() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();

        Assertions.assertTrue(customArray.add(111));
        Assertions.assertEquals(111, customArray.get(customArray.size() - 1));
        Assertions.assertTrue(customArray.add(222));
        Assertions.assertEquals(222, customArray.get(customArray.size() - 1));
        Assertions.assertEquals(2, customArray.size());
    }

    /**
     * Добавление элементов через
     * - добавление массива
     * - добавление коллекции
     * - добавление массива с индекса
     */
    @Test
    public void addAllTest() {
        //Добавление массива
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.addAll(new Integer[] {111, 222, 333, 444, 555});

        Assertions.assertEquals(444, customArray.get(3));

        //Добавление коллекции
        CustomArrayImpl<Integer> customArray2 = new CustomArrayImpl<>();
        List<Integer> list = new ArrayList<>();
        list.add(111);
        list.add(222);
        list.add(333);
        list.add(444);
        list.add(555);
        customArray2.addAll(list);

        Assertions.assertEquals(222, customArray2.get(1));

        //Добавление массива с индекса
        CustomArrayImpl<Integer> customArray3 = new CustomArrayImpl<>();
        customArray3.addAll(new Integer[] {111, 222, 333});

        Integer[] testArray = new Integer[] {444, 555, 666, 777, 888, 999};
        customArray3.addAll(2, testArray);

//        Assertions.assertEquals(7, customArray3.size());
        Assertions.assertEquals("[ 111 222 333 666 777 888 999 ]", customArray3.toString());
    }

    /**
     * Получения элемента по индексу
     */
    @Test
    public void getTest() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(111);
        customArray.add(222);
        customArray.add(333);

        Assertions.assertEquals(222, customArray.get(1));
    }

    /**
     * Замена элемента по индексу
     */
    @Test
    public void setTest() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(111);
        customArray.add(222);
        customArray.add(333);

        Assertions.assertEquals(222, customArray.set(1, 555));
        Assertions.assertEquals(555, customArray.get(1));
    }

    /**
     * Удаление элемента
     * - по индексу
     * - по значению
     */
    @Test
    public void removeTest() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(111);
        customArray.add(222);
        customArray.add(333);

        customArray.remove(1);
        Assertions.assertEquals(2, customArray.size());
        Assertions.assertEquals(333, customArray.get(1));

        customArray.add(444);
        customArray.add(555);

        Assertions.assertTrue(customArray.remove((Integer) 333));
        Assertions.assertFalse(customArray.remove((Integer) 777));
        Assertions.assertEquals(3, customArray.size());
        Assertions.assertEquals(444, customArray.get(1));
    }

    /**
     * Наличие элемента
     */
    @Test
    public void containsTest() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(111);
        customArray.add(222);
        customArray.add(333);

        Assertions.assertFalse(customArray.contains(777));
        Assertions.assertTrue(customArray.contains(111));
    }

    /**
     * Поиск индекса
     */
    @Test
    public void indexOfTest() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(111);
        customArray.add(222);
        customArray.add(333);

        Assertions.assertEquals(0, customArray.indexOf(111));
        Assertions.assertEquals(2, customArray.indexOf(333));
    }

    /**
     * Гарантированный объем
     * - 20
     * - 3
     * - заполняем массив 3 элементами
     * - оставляем емкость 1
     */
    @Test
    public void ensureCapacityTest() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.ensureCapacity(20);

        Assertions.assertEquals(20, customArray.getCapacity());

        customArray.ensureCapacity(3);

        customArray.add(111);
        customArray.add(222);
        customArray.add(333);

        Assertions.assertEquals(3, customArray.size());

        customArray.ensureCapacity(1);

        Assertions.assertEquals(1, customArray.getCapacity());
        Assertions.assertEquals(1, customArray.size());
    }

    /**
     * Объем массива
     * - пустой
     * - с указанным объемом (33)
     */
    @Test
    public void getCapacityTest() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();

        Assertions.assertEquals(0, customArray.getCapacity());

        CustomArrayImpl<Integer> customArray2 = new CustomArrayImpl<>(33);

        Assertions.assertEquals(33, customArray2.getCapacity());
    }

    /**
     * Реверс
     */
    @Test
    public void revers() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(111);
        customArray.add(222);
        customArray.add(333);
        customArray.add(444);
        customArray.add(555);

        customArray.reverse();
        Assertions.assertEquals(555, customArray.get(0));
        Assertions.assertEquals(444, customArray.get(1));
        Assertions.assertEquals(333, customArray.get(2));
        Assertions.assertEquals(222, customArray.get(3));
        Assertions.assertEquals(111, customArray.get(4));
    }

    /**
     * toString
     */
    @Test
    public void toStringTest() {
        CustomArrayImpl<Integer> customArray = new CustomArrayImpl<>();
        customArray.add(111);
        customArray.add(222);
        customArray.add(333);
        customArray.add(444);
        customArray.add(555);

        Assertions.assertEquals("[ 111 222 333 444 555 ]", customArray.toString());
    }
}
