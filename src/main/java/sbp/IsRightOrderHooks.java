package sbp;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nochkin Evgeniy
 * @version 1.0
 */
public class IsRightOrderHooks {
    private final static char a1 = 40;
    private final static char a2 = 41;
    private final static char b1 = 91;
    private final static char b2 = 93;
    private final static char c1 = 123;
    private final static char c2 = 125;

    /**
     * Проверка на правильное содержание скобок в строке
     * @param str - строка состоящая из скобок "(){}[]"
     * @return - возвращает true. если строка состоит
     * из скобок и они пеоеданы в корректной последовательности
     */
    public static boolean isRightOrderHooksMethod(String str) {
        if (str.length() == 0) return false;

        char[] chars = str.toCharArray();

        for (char s : chars) {
            if (s != a1 & s != a2 & s != b1 & s != b2 & s != c1 & s != c2) return false;
        }

        List<Character> list = new ArrayList<>();
        list.add(chars[0]);

        for (int i = 1; i < chars.length; i++) {
            if (chars[i] == a1 || chars[i] == b1 || chars[i] == c1) {
                list.add(chars[i]);
            } else {
                if (list.isEmpty()) return false;

                if ((chars[i] == a2 & list.get(list.size() - 1) == a1) ||
                        (chars[i] == b2 & list.get(list.size() - 1) == b1) ||
                        (chars[i] == c2 & list.get(list.size() - 1) == c1)) {
                    list.remove(list.size() - 1);
                } else {
                    return false;
                }
            }
        }


        return list.isEmpty();
    }
}