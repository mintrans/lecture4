package sbp.comparators;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class CustomDigitComparatorTest {

    /**
     * Тест сортировкаи List с кастомным компаратором
     */
    @Test
    public void compareTestNotNull() {

        List<Integer> list = new ArrayList<>();

        list.add(-7);
        list.add(5);
        list.add(-1);
        list.add(0);
        list.add(2);

        Comparator<Integer> comparable = new CustomDigitComparator();
        list.sort(comparable);

        System.out.println(list);

        Assertions.assertEquals("[0, 2, -7, -1, 5]", list.toString());
    }
}
