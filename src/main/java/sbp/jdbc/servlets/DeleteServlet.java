package sbp.jdbc.servlets;

import sbp.jdbc.dao.PersonRepo;
import sbp.jdbc.dao.PersonRepoImpl;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Nochkin Evgeniy
 * @version 1.0
 * Сервлет удаления пользователя
 */
@WebServlet("/person/delete")
public class DeleteServlet extends HttpServlet {

    PersonRepo personRepo = new PersonRepoImpl();

    /**
     * Обработка GET запроса.
     * Удаляет пользователя из базы данных по ID,
     * с использованием {@link sbp.jdbc.dao.PersonRepo}.
     * После удаления возвращает на стартовую страницу.
     * @param req
     * @param resp
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        int id = Integer.parseInt(req.getParameter("id"));
        personRepo.deletePerson(id);
        try {
            resp.sendRedirect("http://localhost:8080/Lecture4/index");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
