package sbp.jdbc.dao;

import org.springframework.stereotype.Repository;
import sbp.jdbc.model.Person;
import sbp.jdbc.service.PersonService;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nochkin Evgeniy
 * @version 1.0
 * Класс CRUD взаимодействия с базой данных класса {@link sbp.jdbc.model.Person}
 */
@Repository
public class PersonRepoImpl implements PersonRepo {

    private PersonService service;

    /**
     * Получение подключения к базе данных
     * и создание табоицы
     */
    public PersonRepoImpl() {
        service = new PersonService();
        createTable();
    }

    /**
     * Создание таблицы persons при ее отсутвии
     */
    private void createTable() {

        final String sqlQuery = "CREATE TABLE IF NOT EXISTS persons (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "personName VARCHAR(32) NOT NULL, city VARCHAR(32) NOT NULL, age INTEGER NOT NULL);";

        if (service.executeUpdate(sqlQuery) > 0) System.out.println("Создана новая таблица.");;
    }

    /**
     * Удаление таблицы persons
     */
    public void deleteTable() {

        final String sqlQuery = "DROP TABLE persons;";

        if (service.executeUpdate(sqlQuery) > 0) System.out.println("Таблица удалена.");;
    }

    /**
     * Добавление объекта Person в базу данных
     * @param person - добавляемый объект Person
     * @return - true при удачном добавлении
     */
    @Override
    public boolean createPerson(Person person) {

        final String sqlQuery = String.format("INSERT INTO persons ('personName', 'city', 'age') " +
                        " VALUES ('%s', '%s', '%d')",
                person.getPersonName(), person.getCity(), person.getAge());

        int result = service.executeUpdate(sqlQuery);

        return result > 0;
    }

    /**
     * Получение списка всех объектов Person из базы данных
     * @return - {@link ArrayList} Person
     */
    @Override
    public List<Person> findAllPerson() {

        final String sqlQuery = "SELECT * FROM persons";
        List<Person> personList = service.executeQuery(sqlQuery);

        if (personList.size() != 0)
            return personList;

        return null;
    }

    /**
     * Получение объекта класса {@link sbp.jdbc.model.Person}
     * из базы данных по id
     * @param id - идентификатор в базе данных
     * @return - найденный {@link sbp.jdbc.model.Person}
     */
    @Override
    public Person findPersonById(int id) {

        final String sqlQuery = "SELECT * FROM persons WHERE id = '" + id + "';";
        List<Person> list = service.executeQuery(sqlQuery);

        if (list.size() != 0)
            return list.get(0);

        return null;
    }

    /**
     * Изменений объекта класса {@link sbp.jdbc.model.Person} по id
     * @param person - новый {@link sbp.jdbc.model.Person}
     * @return - true если изменение прошло успешно
     */
    @Override
    public boolean updatePerson(Person person) {

        final String sqlQuery = String.format("update persons set personName = '%s', city = '%s', age = '%d' where id = '%d';",
                    person.getPersonName(), person.getCity(), person.getAge(), person.getId());

        int result = service.executeUpdate(sqlQuery);

        return result > 0;
    }

    /**
     * Удаление объекта класса {@link sbp.jdbc.model.Person} по id
     * @param id - идентификатор в базе данных
     * @return - true если удаление прошло успешно
     */
    @Override
    public boolean deletePerson(int id) {

        final String sqlQuery = "delete from persons where id = '" + id + "';";

        int result = service.executeUpdate(sqlQuery);

        return result > 0;
    }
}
