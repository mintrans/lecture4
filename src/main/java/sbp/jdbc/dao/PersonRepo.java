package sbp.jdbc.dao;

import sbp.jdbc.model.Person;

import java.util.List;

public interface PersonRepo {
    boolean createPerson(Person person);
    List<Person> findAllPerson();
    Person findPersonById(int id);
    boolean updatePerson(Person person);
    boolean deletePerson(int id);
}
