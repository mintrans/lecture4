package sbp.exception;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import sbp.exceptions.MyException;
import sbp.exceptions.WorkWithExceptions;

public class WorkWithExceptionTest {

    /**
     * Тестирование метода на пробрасование кастомного исключения
     * @throws Exception
     */
    @Test
    public void exceptionProcessingTest() throws Exception
    {
        WorkWithExceptions workWithExceptions = new WorkWithExceptions();

        Throwable throwable = Assertions.assertThrows(MyException.class, () -> { workWithExceptions.exceptionProcessing(); });
    }
}
