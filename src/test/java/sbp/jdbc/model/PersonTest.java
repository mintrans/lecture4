package sbp.jdbc.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sbp.jdbc.model.Person;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PersonTest {

    /**
     * Тест методов equals и hashCode класса Person
     */
    @Test
    public void equalsAndHashCodeTest() {
        Person person1 = new Person("Vasiliy", "Rostov", 40);
        Person person2 = person1;
        Person person3 = new Person("Vasiliy", "Rostov", 40);
        Person person4 = new Person("Vasiliy", "Rostov", 39);

        Assertions.assertTrue(person1.equals(person2));
        Assertions.assertTrue(person1.hashCode() == person2.hashCode());

        Assertions.assertTrue(person1.equals(person3));
        Assertions.assertTrue(person1.hashCode() == person3.hashCode());

        Assertions.assertTrue(person2.equals(person3));
        Assertions.assertTrue(person2.hashCode() == person3.hashCode());

        Assertions.assertFalse(person1.equals(person4));
        Assertions.assertFalse(person1.hashCode() == person4.hashCode());
    }
}
