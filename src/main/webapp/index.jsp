<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
    <head>
        <style>
            <%@ include file="css/style.css"%>
        </style>
        <link rel="stylesheet" type="text/css" href="${request.contextPath}/css/style.css" />
        <title></title>
    </head>

    <body>
        <h1>Хакатон поклонников Пика Балмера</h1>
        <br>
        <h3>История</h3>
        <div>
            Стив Балмер — бывший гендиректор Майкрософт, под руководством которого вышли первые версии Windows и MS-DOS.
            Каждое выступление его было наполнено эпатажем.
            Стив Балмер любил создавать ажиотаж. В 80-х годах поводом стала его придуманная закономерность — Пик Балмера. Каким-то образом Балмер посчитал, что 1,29–1,38‰ алкоголя в крови открывают у него сверхчеловеческие способности к программированию: он выпивал пару литров пива, входил в состояние лёгкого опьянения и мог продуктивно кодить.
            Свою закономерность Стив Балмер привязывал к особенностям центральной нервной системы и психологическому закону Йеркса — Додсона: это когда оптимальная работоспособность достигается при среднем уровне мотивации. Если зациклиться на задаче, то вместо роста продуктивности наступает эмоциональное напряжение, и это мешает работе — Балмер не любил напрягаться и предпочитал всегда оставаться в расслабленном состоянии.
        </div>
        <hr>
        <div id="tabl">
            <form action="Edit" method="post">
                <div aling="center">
                    <table border="1" cellpadding="5">
                        <caption><h2>Список участников:</h2></caption>
                        <tr>
                            <th>ID</th>
                            <th>Имя</th>
                            <th>Город</th>
                            <th>Возраст</th>
                            <th>Редактировать</th>
                            <th>Удалить</th>
                        </tr>
                        <c:forEach var="person" items="${personsList}">
                            <tr>
                                <td><c:out value="${person.id}" /></td>
                                <td><c:out value="${person.personName}" /></td>
                                <td><c:out value="${person.city}" /></td>
                                <td><c:out value="${person.age}" /></td>
                                <td><a href="${pageContext.servletContext.contextPath}/person/edit?id=${person.id}">Редактировать</a></td>
                                <td><a href="${pageContext.servletContext.contextPath}/person/delete?id=${person.id}">Удалить</a></td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <br>
                <td><a href="${pageContext.servletContext.contextPath}/person/add">Добавить пользователя</a></td>
            </form>
        </div>
        <hr>
        <td><a href="about.jsp">О сайте</a></td>
    </body>
</html>