package sbp.jdbc.servlets;

import sbp.jdbc.dao.PersonRepo;
import sbp.jdbc.dao.PersonRepoImpl;
import sbp.jdbc.model.Person;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Nochkin Evgeniy
 * @version 1.0
 * Сервлет добавления нового пользователя
 */
@WebServlet("/person/add")
public class AddServlet extends HttpServlet {

    PersonRepo personRepo = new PersonRepoImpl();

    /**
     * Обработка GET запроса.
     * Перенаправляет на страницу редактирования пользователя.
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("person.jsp").forward(req, resp);
    }

    /**
     * Обработка POST запроса.
     * Создание нового пользователя в базе данных
     * с использованием {@link sbp.jdbc.dao.PersonRepo}.
     * После добавление возвращает на стартовую страницу.
     * @param req
     * @param resp
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        String personName = req.getParameter("personName");
        String city = req.getParameter("city");
        int age = Integer.parseInt(req.getParameter("age"));

        Person person = new Person(personName, city, age);
        personRepo.createPerson(person);

        try {
            resp.sendRedirect("http://localhost:8080/Lecture4/index");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
